package grunt009.torchmod.client;

/*
* Grunt009's Colored Torches
* Author: grunt009
* License: Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
*/

import grunt009.torchmod.CommonProxy;
import net.minecraftforge.client.MinecraftForgeClient;


public class ClientProxy extends CommonProxy {
    @Override
    public void registerRenderers() {
    }
}
