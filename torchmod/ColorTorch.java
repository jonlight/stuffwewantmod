package grunt009.torchmod;

import java.util.Random;

import net.minecraft.block.BlockTorch;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.*;
import net.minecraft.client.renderer.RenderEngine;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import grunt009.torchmod.client.particle.EntityColorFlameFX;

/*
* Grunt009's Colored Torches
* Author: grunt009
* License: Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
*/

public class ColorTorch extends BlockTorch {
	
	public float f1 = 1.0F;
	public float f2 = 1.0F;
	public float f3 = 1.0F;
	
	public ColorTorch(int par1) {
        super(par1);
        this.setTickRandomly(true);
        this.setCreativeTab(CreativeTabs.tabDecorations);
	}
	
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister iconRegister)
	{
	         this.blockIcon = iconRegister.registerIcon("Torchmod:" + this.getUnlocalizedName().substring(5));
	}
	
	public int idDropped(int par1, Random par2Random, int par3){
		return this.blockID;
	}
	
    public int quantityDropped(Random par1Random)
    {
        return 1;
    }
    
    public void setColor(float par1, float par2, float par3){
    	this.f1=par1;
    	this.f2=par2;
    	this.f3=par3;
    }
	
    @SideOnly(Side.CLIENT)

    /**
     * A randomly called display update to be able to add particles or other items for display
     */
    public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {
        int l = par1World.getBlockMetadata(par2, par3, par4);
        double d0 = (double)((float)par2 + 0.5F);
        double d1 = (double)((float)par3 + 0.7F);
        double d2 = (double)((float)par4 + 0.5F);
        double d3 = 0.2199999988079071D;
        double d4 = 0.27000001072883606D;

        if (l == 1)
        {
            par1World.spawnParticle("smoke", d0 - d4, d1 + d3, d2, 0.0D, 0.0D, 0.0D);
            //par1World.spawnParticle("flame", d0 - d4, d1 + d3, d2, 0.0D, 0.0D, 0.0D);
            EntityFX colorFlame = new EntityColorFlameFX(par1World, d0 - d4, d1 + d3, d2, 0.0D, 0.0D, 0.0D);
            colorFlame.setRBGColorF(this.f1, this.f2, this.f3);
            Minecraft.getMinecraft().effectRenderer.addEffect(colorFlame);
        }
        else if (l == 2)
        {
            par1World.spawnParticle("smoke", d0 + d4, d1 + d3, d2, 0.0D, 0.0D, 0.0D);
            //par1World.spawnParticle("flame", d0 + d4, d1 + d3, d2, 0.0D, 0.0D, 0.0D);
            EntityFX colorFlame = new EntityColorFlameFX(par1World, d0 + d4, d1 + d3, d2, 0.0D, 0.0D, 0.0D);
            colorFlame.setRBGColorF(this.f1, this.f2, this.f3);
            Minecraft.getMinecraft().effectRenderer.addEffect(colorFlame);
        }
        else if (l == 3)
        {
            par1World.spawnParticle("smoke", d0, d1 + d3, d2 - d4, 0.0D, 0.0D, 0.0D);
            //par1World.spawnParticle("flame", d0, d1 + d3, d2 - d4, 0.0D, 0.0D, 0.0D);
            EntityFX colorFlame = new EntityColorFlameFX(par1World, d0, d1 + d3, d2 - d4, 0.0D, 0.0D, 0.0D);
            colorFlame.setRBGColorF(this.f1, this.f2, this.f3);
            Minecraft.getMinecraft().effectRenderer.addEffect(colorFlame);
        }
        else if (l == 4)
        {
            par1World.spawnParticle("smoke", d0, d1 + d3, d2 + d4, 0.0D, 0.0D, 0.0D);
            //par1World.spawnParticle("flame", d0, d1 + d3, d2 + d4, 0.0D, 0.0D, 0.0D);
            EntityFX colorFlame = new EntityColorFlameFX(par1World, d0, d1 + d3, d2 + d4, 0.0D, 0.0D, 0.0D);
            colorFlame.setRBGColorF(this.f1, this.f2, this.f3);
            Minecraft.getMinecraft().effectRenderer.addEffect(colorFlame);
        }
        else
        {
            par1World.spawnParticle("smoke", d0, d1, d2, 0.0D, 0.0D, 0.0D);
            //par1World.spawnParticle("flame", d0, d1, d2, 0.0D, 0.0D, 0.0D);
            EntityFX colorFlame = new EntityColorFlameFX(par1World, d0, d1, d2, 0.0D, 0.0D, 0.0D);
            colorFlame.setRBGColorF(this.f1, this.f2, this.f3);
            Minecraft.getMinecraft().effectRenderer.addEffect(colorFlame);
        }
    }

}
