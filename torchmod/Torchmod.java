package grunt009.torchmod;

import net.minecraft.block.Block;
import net.minecraft.block.BlockTorch;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import grunt009.torchmod.client.particle.EntityColorFlameFX;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

/*
* Grunt009's Colored Torches
* Author(s): grunt009
* License: Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
*/

@Mod(modid="Torchmod", name="Grunt009's Colored Torches", version="1.2.1")
@NetworkMod(clientSideRequired=true, serverSideRequired=false)
public class Torchmod {

        // The instance of your mod that Forge uses.
        @Instance("Torchmod")
        public static Torchmod instance;
       
        public static final Block torchColor0 = (new ColorTorch(4080)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor0");
		public static final Block torchColor1 = (new ColorTorch(4081)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor1");
		public static final Block torchColor2 = (new ColorTorch(4082)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor2");
		public static final Block torchColor3 = (new ColorTorch(4083)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor3");
		public static final Block torchColor4 = (new ColorTorch(4084)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor4");
		public static final Block torchColor5 = (new ColorTorch(4085)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor5");
		public static final Block torchColor6 = (new ColorTorch(4086)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor6");
		public static final Block torchColor7 = (new ColorTorch(4087)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor7");
		public static final Block torchColor8 = (new ColorTorch(4088)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor8");
		public static final Block torchColor9 = (new ColorTorch(4089)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor9");
		public static final Block torchColor10 = (new ColorTorch(4090)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor10");
		public static final Block torchColor11 = (new ColorTorch(4091)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor11");
		public static final Block torchColor12 = (new ColorTorch(4092)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor12");
		public static final Block torchColor13 = (new ColorTorch(4093)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor13");
		public static final Block torchColor14 = (new ColorTorch(4094)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor14");
		public static final Block torchColor15 = (new ColorTorch(4095)).setHardness(0.0F).setLightValue(0.9375F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("torchColor15");
		
		
        // Says where the client and server 'proxy' code is loaded.
        @SidedProxy(clientSide="grunt009.torchmod.client.ClientProxy", serverSide="grunt009.torchmod.CommonProxy")
        public static CommonProxy proxy;
       
        @PreInit
        public void preInit(FMLPreInitializationEvent event) {
                // Stub Method
        }
       
        @Init
        public void load(FMLInitializationEvent event) {       		
        	
        		EntityRegistry.registerModEntity(EntityColorFlameFX.class, "ColorFlameFX", 174, instance, 50, 5, true);
        	
        		GameRegistry.registerBlock(torchColor0, "Generic" + torchColor0.getUnlocalizedName().substring(5));  
        		GameRegistry.registerBlock(torchColor1, "Generic" + torchColor1.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor2, "Generic" + torchColor2.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor3, "Generic" + torchColor3.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor4, "Generic" + torchColor4.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor5, "Generic" + torchColor5.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor6, "Generic" + torchColor6.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor7, "Generic" + torchColor7.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor8, "Generic" + torchColor8.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor9, "Generic" + torchColor9.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor10, "Generic" + torchColor10.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor11, "Generic" + torchColor11.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor12, "Generic" + torchColor12.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor13, "Generic" + torchColor13.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor14, "Generic" + torchColor14.getUnlocalizedName().substring(5));
        		GameRegistry.registerBlock(torchColor15, "Generic" + torchColor15.getUnlocalizedName().substring(5));
        		
        		LanguageRegistry.addName(torchColor0, "Black Torch");
        		LanguageRegistry.addName(torchColor1, "Red Torch");
        		LanguageRegistry.addName(torchColor2, "Green Torch");
        		LanguageRegistry.addName(torchColor3, "Brown Torch");
        		LanguageRegistry.addName(torchColor4, "Blue Torch");
        		LanguageRegistry.addName(torchColor5, "PurpleTorch");
        		LanguageRegistry.addName(torchColor6, "Cyan Torch");
        		LanguageRegistry.addName(torchColor7, "Light Gray Torch");
        		LanguageRegistry.addName(torchColor8, "Gray Torch");
        		LanguageRegistry.addName(torchColor9, "Pink Torch");
        		LanguageRegistry.addName(torchColor10, "Lime Torch");
        		LanguageRegistry.addName(torchColor11, "Yellow Torch");
        		LanguageRegistry.addName(torchColor12, "Light Blue Torch");
        		LanguageRegistry.addName(torchColor13, "Magenta Torch");
        		LanguageRegistry.addName(torchColor14, "Orange Torch");
        		LanguageRegistry.addName(torchColor15, "White Torch");
        		
        		GameRegistry.addRecipe(new ItemStack(torchColor0, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 0)});
        		GameRegistry.addRecipe(new ItemStack(torchColor1, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 1)});
        		GameRegistry.addRecipe(new ItemStack(torchColor2, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 2)});
        		GameRegistry.addRecipe(new ItemStack(torchColor3, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 3)});
        		GameRegistry.addRecipe(new ItemStack(torchColor4, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 4)});
        		GameRegistry.addRecipe(new ItemStack(torchColor5, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 5)});
        		GameRegistry.addRecipe(new ItemStack(torchColor6, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 6)});
        		GameRegistry.addRecipe(new ItemStack(torchColor7, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 7)});
        		GameRegistry.addRecipe(new ItemStack(torchColor8, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 8)});
        		GameRegistry.addRecipe(new ItemStack(torchColor9, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 9)});
        		GameRegistry.addRecipe(new ItemStack(torchColor10, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 10)});
        		GameRegistry.addRecipe(new ItemStack(torchColor11, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 11)});
        		GameRegistry.addRecipe(new ItemStack(torchColor12, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 12)});
        		GameRegistry.addRecipe(new ItemStack(torchColor13, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 13)});
        		GameRegistry.addRecipe(new ItemStack(torchColor14, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 14)});
        		GameRegistry.addRecipe(new ItemStack(torchColor15, 4), new Object[] {"XD", "# ", 'X', Item.coal, '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 15)});
        		
        		GameRegistry.addRecipe(new ItemStack(torchColor0, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 0)});
        		GameRegistry.addRecipe(new ItemStack(torchColor1, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 1)});
        		GameRegistry.addRecipe(new ItemStack(torchColor2, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 2)});
        		GameRegistry.addRecipe(new ItemStack(torchColor3, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 3)});
        		GameRegistry.addRecipe(new ItemStack(torchColor4, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 4)});
        		GameRegistry.addRecipe(new ItemStack(torchColor5, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 5)});
        		GameRegistry.addRecipe(new ItemStack(torchColor6, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 6)});
        		GameRegistry.addRecipe(new ItemStack(torchColor7, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 7)});
        		GameRegistry.addRecipe(new ItemStack(torchColor8, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 8)});
        		GameRegistry.addRecipe(new ItemStack(torchColor9, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 9)});
        		GameRegistry.addRecipe(new ItemStack(torchColor10, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 10)});
        		GameRegistry.addRecipe(new ItemStack(torchColor11, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 11)});
        		GameRegistry.addRecipe(new ItemStack(torchColor12, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 12)});
        		GameRegistry.addRecipe(new ItemStack(torchColor13, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 13)});
        		GameRegistry.addRecipe(new ItemStack(torchColor14, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 14)});
        		GameRegistry.addRecipe(new ItemStack(torchColor15, 4), new Object[] {"XD", "# ", 'X', new ItemStack(Item.coal, 1, 1), '#', Item.stick, 'D', new ItemStack(Item.dyePowder, 1, 15)});
        
        		((ColorTorch) torchColor0).setColor(0.0F, 0.0F, 0.0F);
        		((ColorTorch) torchColor1).setColor(1.0F, 0.0F, 0.0F);
        		((ColorTorch) torchColor2).setColor(0.0F, 0.502F, 0.0F);
        		((ColorTorch) torchColor3).setColor(0.54F, 0.27F, 0.075F);
        		((ColorTorch) torchColor4).setColor(0.0F, 0.0F, 1.0F);
        		((ColorTorch) torchColor5).setColor(0.8F, 0.0F, 0.8F);
        		((ColorTorch) torchColor6).setColor(0.0F, 1.0F, 1.0F);
        		((ColorTorch) torchColor7).setColor(0.753F, 0.753F, 0.753F);
        		((ColorTorch) torchColor8).setColor(0.502F, 0.502F, 0.502F);
        		((ColorTorch) torchColor9).setColor(1.0F, 0.753F, 0.796F);
        		((ColorTorch) torchColor10).setColor(0.0F, 1.0F, 0.0F);
        		((ColorTorch) torchColor11).setColor(1.0F, 1.0F, 0.0F);
        		((ColorTorch) torchColor12).setColor(0.678F, 0.847F, 0.902F);
        		((ColorTorch) torchColor13).setColor(1.0F, 0.0F, 1.0F);
        		((ColorTorch) torchColor14).setColor(1.0F, 0.647F, 0.0F);
        		((ColorTorch) torchColor15).setColor(1.0F, 1.0F, 1.0F);
        }
       
        @PostInit
        public void postInit(FMLPostInitializationEvent event) {
                // Stub Method
        }
        
        
		
		
		
		
}